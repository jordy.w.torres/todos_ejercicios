----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:09:29 05/18/2022 
-- Design Name: 
-- Module Name:    Codigo_morse - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Codigo_Morse is
    Port ( button  : in  STD_LOGIC;
           senial : out  std_logic);
end Codigo_Morse;
 
architecture Behavioral of Codigo_Morse is
begin
process (button) begin 
if button = '1' then 
senial <= '1';
else senial <= '0';
end if;
end process;
end Behavioral;