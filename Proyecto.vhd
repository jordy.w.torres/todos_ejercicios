----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:34:46 05/17/2022 
-- Design Name: 
-- Module Name:    Proyecto - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity entidad is
Port ( a,b: in  STD_LOGIC_VECTOR(0 to 3);
			  F : out  STD_LOGIC);
end entidad;

architecture Behavioral of entidad is
begin
process(a,b) begin 
if(((a(0) or b(0)) = '0') or ((a(1) or b(1)) = '0') or ((a(2) or b(2)) = '0') or ((a(3) or b(3)) = '0') ) then
F <= '0';
else F <= '1';
end if;
end process;
end Behavioral;

